-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 29-Jan-2021 às 12:17
-- Versão do servidor: 10.4.11-MariaDB
-- versão do PHP: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `loja`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias`
--

CREATE TABLE `categorias` (
  `categoria_id` int(11) NOT NULL,
  `categoria_pai_id` int(11) DEFAULT NULL,
  `categoria_nome` varchar(45) NOT NULL,
  `categoria_ativa` tinyint(1) DEFAULT NULL,
  `categoria_meta_link` varchar(100) DEFAULT NULL,
  `categoria_data_criacao` timestamp NOT NULL DEFAULT current_timestamp(),
  `categoria_data_alteracao` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `categorias`
--

INSERT INTO `categorias` (`categoria_id`, `categoria_pai_id`, `categoria_nome`, `categoria_ativa`, `categoria_meta_link`, `categoria_data_criacao`, `categoria_data_alteracao`) VALUES
(1, 1, 'Notebook', 1, 'notebook', '2021-01-12 04:01:59', '2021-01-21 15:28:46'),
(2, 1, 'Computadores', 1, 'computadores', '2021-01-12 19:46:58', '2021-01-20 00:47:05'),
(4, 4, 'Mouses', 1, 'mouses', '2021-01-13 01:09:37', '2021-01-20 00:47:22'),
(5, 6, 'Head-Set', 1, 'head-set', '2021-01-16 01:23:14', '2021-01-20 00:47:35'),
(6, 4, 'Teclado', 1, 'teclado', '2021-01-16 01:24:18', '2021-01-20 00:41:53'),
(7, 7, 'Play Station', 1, 'play-station', '2021-01-20 00:56:52', '2021-01-20 00:56:52'),
(8, 4, 'Acessórios', 1, 'acessorios', '2021-01-20 01:03:43', '2021-01-20 01:03:43'),
(9, 8, 'Impressoras', 1, 'impressoras', '2021-01-20 01:06:38', '2021-01-20 01:06:38'),
(10, 2, 'Eletrônicos', 1, 'eletronicos', '2021-01-21 15:30:28', '2021-01-21 15:30:28');

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias_pai`
--

CREATE TABLE `categorias_pai` (
  `categoria_pai_id` int(11) NOT NULL,
  `categoria_pai_nome` varchar(45) NOT NULL,
  `categoria_pai_ativa` tinyint(1) DEFAULT NULL,
  `categoria_pai_meta_link` varchar(100) DEFAULT NULL,
  `categoria_pai_data_criacao` timestamp NOT NULL DEFAULT current_timestamp(),
  `categoria_pai_data_alteracao` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `categorias_pai`
--

INSERT INTO `categorias_pai` (`categoria_pai_id`, `categoria_pai_nome`, `categoria_pai_ativa`, `categoria_pai_meta_link`, `categoria_pai_data_criacao`, `categoria_pai_data_alteracao`) VALUES
(1, 'Informática', 1, 'informatica', '2021-01-11 23:07:15', '2021-01-11 23:07:15'),
(2, 'Eletrônicos', 1, 'eletronicos', '2021-01-12 01:25:02', '2021-01-12 01:25:02'),
(4, 'Acessórios', 1, 'acessorios', '2021-01-12 15:25:59', '2021-01-12 15:25:59'),
(5, 'Jogos', 1, 'jogos', '2021-01-12 15:26:19', '2021-01-12 15:26:19'),
(6, 'Musicas', 1, 'musicas', '2021-01-20 00:34:19', '2021-01-20 00:34:19'),
(7, 'Consoles', 1, 'consoles', '2021-01-20 00:56:12', '2021-01-20 00:56:12'),
(8, 'Impressora', 1, 'impressora', '2021-01-20 01:06:21', '2021-01-20 01:06:21');

-- --------------------------------------------------------

--
-- Estrutura da tabela `clientes`
--

CREATE TABLE `clientes` (
  `cliente_id` int(11) NOT NULL,
  `cliente_data_cadastro` timestamp NULL DEFAULT current_timestamp(),
  `cliente_nome` varchar(45) NOT NULL,
  `cliente_sobrenome` varchar(150) NOT NULL,
  `cliente_data_nascimento` date DEFAULT NULL,
  `cliente_cpf` varchar(20) NOT NULL,
  `cliente_telefone_fixo` varchar(20) DEFAULT NULL,
  `cliente_telefone_movel` varchar(20) NOT NULL,
  `cliente_cep` varchar(10) NOT NULL,
  `cliente_endereco` varchar(155) NOT NULL,
  `cliente_numero_endereco` varchar(20) DEFAULT NULL,
  `cliente_bairro` varchar(45) NOT NULL,
  `cliente_cidade` varchar(105) NOT NULL,
  `cliente_estado` varchar(2) NOT NULL,
  `cliente_complemento` varchar(145) DEFAULT NULL,
  `cliente_data_alteracao` timestamp NULL DEFAULT current_timestamp(),
  `cliente_email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `clientes`
--

INSERT INTO `clientes` (`cliente_id`, `cliente_data_cadastro`, `cliente_nome`, `cliente_sobrenome`, `cliente_data_nascimento`, `cliente_cpf`, `cliente_telefone_fixo`, `cliente_telefone_movel`, `cliente_cep`, `cliente_endereco`, `cliente_numero_endereco`, `cliente_bairro`, `cliente_cidade`, `cliente_estado`, `cliente_complemento`, `cliente_data_alteracao`, `cliente_email`) VALUES
(1, '2021-01-19 01:35:46', 'Rafael', 'Guilherme', '1989-09-04', '363.197.998-38', '(11) 2721-2352', '(11) 94570-2236', '03934-030', 'Rua Lagoa Bonita', '5', 'Jardim Imperador', 'São Paulo', 'SP', '', '2021-01-19 01:35:46', 'fael3_fael@yahoo.com.br'),
(2, '2021-01-19 01:37:53', 'Yasmin', 'Souza', '1994-12-18', '422.104.180-38', '(11) 6721-2352', '(11) 99199-9935', '03934-040', 'Rua São Roque', '38', 'Jardim Imperador', 'São Paulo', 'SP', '', '2021-01-19 01:37:53', 'yasmin.souza@gmail.com'),
(3, '2021-01-27 16:36:12', 'Renata', 'Fonseca', '1994-06-02', '876.653.330-95', '(55) 1127-2123', '(11) 94570-2235', '03934-030', 'R Lga Bonita', '35', 'Jardim Imperador', 'São Paulo', 'PB', '', '2021-01-27 16:36:12', 'renata@gmail.com'),
(4, '2021-01-27 18:02:01', 'Freya', 'Vormir', '1995-07-03', '982.772.460-64', '(31) 6721-2352', '(31) 99735-2458', '03934-040', 'Rua São João do Paraiso', '30', 'Jardim Imperador', 'São Paulo', 'SP', 'Próximo ao shopping', '2021-01-27 18:02:01', 'freya@gmail.com'),
(6, '2021-01-28 16:34:54', 'Priscila', 'Romero', '1991-02-28', '216.136.280-13', NULL, '(11) 94570-2232', '03934-040', 'R Lga Bonita', '5', 'Jardim Imperador (Zona Leste)', 'São Paulo', 'SP', NULL, '2021-01-28 16:34:54', 'priscila@gmail.com'),
(8, '2021-01-28 18:02:51', 'Paula', 'Souza', '1990-12-28', '783.553.570-00', NULL, '(55) 11272-1235', '03934-030', 'Rua Lagoa Bonita', '5', 'Jardim Imperador (Zona Leste)', 'São Paulo', 'SP', NULL, '2021-01-28 18:02:51', '2paula@gmail.com');

-- --------------------------------------------------------

--
-- Estrutura da tabela `config_correios`
--

CREATE TABLE `config_correios` (
  `config_id` int(11) NOT NULL,
  `config_cep_origem` varchar(20) NOT NULL,
  `config_codigo_pac` varchar(10) NOT NULL,
  `config_codigo_sedex` varchar(10) NOT NULL,
  `config_somar_frete` decimal(10,2) NOT NULL,
  `config_valor_declarado` decimal(5,2) NOT NULL,
  `config_data_alteracao` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `config_correios`
--

INSERT INTO `config_correios` (`config_id`, `config_cep_origem`, `config_codigo_pac`, `config_codigo_sedex`, `config_somar_frete`, `config_valor_declarado`, `config_data_alteracao`) VALUES
(1, '80530-000', '04510', '04014', '3.50', '21.50', '2021-01-21 15:48:58');

-- --------------------------------------------------------

--
-- Estrutura da tabela `config_pagseguro`
--

CREATE TABLE `config_pagseguro` (
  `config_id` int(11) NOT NULL,
  `config_email` varchar(255) NOT NULL,
  `config_token` varchar(100) NOT NULL,
  `config_ambiente` tinyint(1) NOT NULL COMMENT '0 -> Ambiente real / 1 -> Ambiente sandbox',
  `config_data_alteracao` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `config_pagseguro`
--

INSERT INTO `config_pagseguro` (`config_id`, `config_email`, `config_token`, `config_ambiente`, `config_data_alteracao`) VALUES
(1, 'fael3_fael@yahoo.com.br', '2267EC207A9A452BB5A4BDDDC295D73C', 1, '2021-01-27 02:51:18');

-- --------------------------------------------------------

--
-- Estrutura da tabela `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'clientes', 'clientes');

-- --------------------------------------------------------

--
-- Estrutura da tabela `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `marcas`
--

CREATE TABLE `marcas` (
  `marca_id` int(11) NOT NULL,
  `marca_nome` varchar(45) NOT NULL,
  `marca_meta_link` varchar(255) NOT NULL,
  `marca_ativa` tinyint(1) DEFAULT NULL,
  `marca_data_criacao` timestamp NOT NULL DEFAULT current_timestamp(),
  `marca_data_alteracao` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `marcas`
--

INSERT INTO `marcas` (`marca_id`, `marca_nome`, `marca_meta_link`, `marca_ativa`, `marca_data_criacao`, `marca_data_alteracao`) VALUES
(1, 'Samsung', 'samsung', 1, '2021-01-08 23:11:55', '2021-01-11 19:12:55'),
(3, 'DELL', 'dell', 1, '2021-01-11 19:18:23', '2021-01-11 19:18:29'),
(4, 'LG', 'lg', 1, '2021-01-15 20:44:35', NULL),
(5, 'APPLE', 'apple', 1, '2021-01-15 20:44:50', '2021-01-16 02:20:47'),
(6, 'Microsoft', 'microsoft', 1, '2021-01-15 20:45:04', '2021-01-16 02:20:53'),
(7, 'Nokia', 'nokia', 1, '2021-01-19 01:41:28', NULL),
(8, 'Motorola', 'motorola', 1, '2021-01-20 00:50:35', NULL),
(9, 'Brastemp', 'brastemp', 1, '2021-01-20 00:51:05', NULL),
(10, 'Sonny', 'sonny', 1, '2021-01-21 15:48:00', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedidos`
--

CREATE TABLE `pedidos` (
  `pedido_id` int(11) NOT NULL,
  `pedido_codigo` varchar(8) DEFAULT NULL,
  `pedido_cliente_id` int(11) DEFAULT NULL,
  `pedido_valor_produtos` decimal(15,2) DEFAULT NULL,
  `pedido_valor_frete` decimal(15,2) DEFAULT NULL,
  `pedido_valor_final` decimal(15,2) DEFAULT NULL,
  `pedido_forma_envio` tinyint(1) DEFAULT NULL COMMENT '1 = Correios Sedex---------------------2 - Correios PAC',
  `pedido_data_cadastro` timestamp NOT NULL DEFAULT current_timestamp(),
  `pedido_data_alteracao` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `pedidos`
--

INSERT INTO `pedidos` (`pedido_id`, `pedido_codigo`, `pedido_cliente_id`, `pedido_valor_produtos`, `pedido_valor_frete`, `pedido_valor_final`, `pedido_forma_envio`, `pedido_data_cadastro`, `pedido_data_alteracao`) VALUES
(1, '64950127', 2, '97.00', '28.32', '125.32', 1, '2021-01-28 17:37:39', NULL),
(2, '03497618', 2, '97.00', '41.72', '138.72', 2, '2021-01-28 17:41:50', NULL),
(3, '02789316', 2, '1500.18', '340.12', '1840.30', 1, '2021-01-28 17:44:49', NULL),
(4, '54012697', 2, '1500.18', '340.12', '1840.30', 1, '2021-01-28 17:47:42', NULL),
(5, '03658921', 2, '1500.18', '340.12', '1840.30', 1, '2021-01-28 18:00:03', NULL),
(6, '48721065', 8, '1500.18', '340.12', '1840.30', 1, '2021-01-28 18:03:21', NULL),
(7, '28465913', 2, '1500.18', '340.12', '1840.30', 1, '2021-01-28 18:06:12', NULL),
(8, '25467013', 2, '97.00', '28.32', '125.32', 1, '2021-01-28 23:43:36', NULL),
(9, '35697412', 2, '97.00', '28.32', '125.32', 1, '2021-01-28 23:48:35', NULL),
(10, '10348752', 2, '97.00', '28.32', '125.32', 1, '2021-01-28 23:52:22', NULL),
(11, '74236051', 2, '97.00', '28.32', '125.32', 1, '2021-01-28 23:55:43', NULL),
(12, '30641795', 2, '1500.18', '340.12', '1840.30', 1, '2021-01-28 23:56:43', NULL),
(13, '61897035', 2, '1500.18', '340.12', '1840.30', 1, '2021-01-29 00:37:27', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedidos_produtos`
--

CREATE TABLE `pedidos_produtos` (
  `pedido_id` int(11) DEFAULT NULL,
  `produto_id` int(11) DEFAULT NULL,
  `produto_nome` varchar(200) NOT NULL,
  `produto_quantidade` int(11) NOT NULL,
  `produto_valor_unitario` decimal(15,2) NOT NULL,
  `produto_valor_total` decimal(15,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `pedidos_produtos`
--

INSERT INTO `pedidos_produtos` (`pedido_id`, `produto_id`, `produto_nome`, `produto_quantidade`, `produto_valor_unitario`, `produto_valor_total`) VALUES
(1, 4, 'Mouse Gamer Laser X7 3200dpi Led Usb 7', 1, '97.00', '97.00'),
(2, 4, 'Mouse Gamer Laser X7 3200dpi Led Usb 7', 1, '97.00', '97.00'),
(3, 12, 'Impressora Laser Hp 1200a', 1, '1500.18', '1500.18'),
(4, 12, 'Impressora Laser Hp 1200a', 1, '1500.18', '1500.18'),
(5, 12, 'Impressora Laser Hp 1200a', 1, '1500.18', '1500.18'),
(6, 12, 'Impressora Laser Hp 1200a', 1, '1500.18', '1500.18'),
(7, 12, 'Impressora Laser Hp 1200a', 1, '1500.18', '1500.18'),
(8, 4, 'Mouse Gamer Laser X7 3200dpi Led Usb 7', 1, '97.00', '97.00'),
(9, 4, 'Mouse Gamer Laser X7 3200dpi Led Usb 7', 1, '97.00', '97.00'),
(10, 4, 'Mouse Gamer Laser X7 3200dpi Led Usb 7', 1, '97.00', '97.00'),
(11, 4, 'Mouse Gamer Laser X7 3200dpi Led Usb 7', 1, '97.00', '97.00'),
(12, 12, 'Impressora Laser Hp 1200a', 1, '1500.18', '1500.18'),
(13, 12, 'Impressora Laser Hp 1200a', 1, '1500.18', '1500.18');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE `produtos` (
  `produto_id` int(11) NOT NULL,
  `produto_codigo` varchar(45) DEFAULT NULL,
  `produto_data_cadastro` timestamp NULL DEFAULT current_timestamp(),
  `produto_categoria_id` int(11) DEFAULT NULL,
  `produto_marca_id` int(11) DEFAULT NULL,
  `produto_nome` varchar(255) DEFAULT NULL,
  `produto_meta_link` varchar(255) DEFAULT NULL,
  `produto_peso` int(11) DEFAULT 0,
  `produto_altura` int(11) DEFAULT 0,
  `produto_largura` int(11) DEFAULT 0,
  `produto_comprimento` int(11) DEFAULT 0,
  `produto_valor` varchar(45) DEFAULT NULL,
  `produto_destaque` tinyint(1) DEFAULT NULL,
  `produto_controlar_estoque` tinyint(1) DEFAULT NULL,
  `produto_quantidade_estoque` int(11) DEFAULT 0,
  `produto_ativo` tinyint(1) DEFAULT NULL,
  `produto_descricao` longtext DEFAULT NULL,
  `produto_data_alteracao` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`produto_id`, `produto_codigo`, `produto_data_cadastro`, `produto_categoria_id`, `produto_marca_id`, `produto_nome`, `produto_meta_link`, `produto_peso`, `produto_altura`, `produto_largura`, `produto_comprimento`, `produto_valor`, `produto_destaque`, `produto_controlar_estoque`, `produto_quantidade_estoque`, `produto_ativo`, `produto_descricao`, `produto_data_alteracao`) VALUES
(4, '16302598', '2021-01-14 16:14:12', 4, 3, 'Mouse Gamer Laser X7 3200dpi Led Usb 7', 'mouse-gamer-laser-x7-3200dpi-led-usb-7', 1, 18, 18, 18, '97.00', 1, 1, 4, 1, 'Conheça o mouse gamer X7 com efeitos visuais em LEDs mude em 3 tores de cor azul, roxo e vermelho, com ajustes DPI que pode ser alterada apenas com um click podendo utilizar nas resoluções 1600/2400/3200dpi', NULL),
(7, '28074631', '2021-01-18 17:04:50', 2, 1, 'Tablet Samsung Galaxy A T295', 'tablet-samsung-galaxy-a-t295', 2, 25, 25, 5, '900.00', 1, 1, 3, 1, 'Descubra um companheiro prático no Galaxy Tab A (8.0”), um tablet que se destaca do básico e acrescenta algo a mais.\r\nFácil de segurar com uma mão, fino, compacto, e portátil, a mistura ideal de desempenho e design.', NULL),
(8, '37521904', '2021-01-20 00:40:43', 6, 5, 'Teclado Gamer Multimídia Blackfire Preto FORTREK G PC/PS4/XBOX', 'teclado-gamer-multimidia-blackfire-preto-fortrek-g-pcps4xbox', 1, 20, 80, 10, '97.00', 1, 1, 3, 1, 'Teclado Gamer Multimídia Blackfire Preto FORTREK G PC/PS4/XBOXOs teclados da Fortrek unem qualidade e beleza em um mesmo produto. O teclado Blackfire é um teclado com design ergonômico e base confeccionada em material resistente. Os...', NULL),
(9, '54391276', '2021-01-20 00:49:01', 2, 4, 'Microfone de Mesa Condensador Mic com Tripé SF-402', 'microfone-de-mesa-condensador-mic-com-tripe-sf-402', 4, 12, 12, 10, '88.96', 1, 1, 5, 1, 'Condensador mic microfone com tripé.\r\nNovo e de alta qualidade\r\npode ser usado para karaokê, sonorização e aplicações multimídia\r\ninstalação rápida e fácil\r\neste conjunto microfone profissional é o ideal para a sala de conferências, distribuição de música e aplicativos de mensagens.\r\nSua ampla resposta de frequência e alta potência fazer performances vocais de som claro e vibrante\r\nportátil, plug and play\r\nnome: microfone condensador\r\nmodelo: sf-666\r\ncorpo material: metal + abs + pvc fio\r\nsuporte material: abs\r\ntamanho: cerca de 134 x 43 x 38 milímetros (microfone)\r\ncaracterísticas:\r\n1 sf-666 microfone condensador é uma produtos de alta-fidelidade sistema de áudio, projetado especificamente para o qq, msn, skype e internet bate-papo passatempo usuário.\r\n2 baixo nível de ruído, adequados para todos os alto-falantes plugue p2.\r\n3 com um tripé, fácil e rápido configurar.\r\nParâmetros técnicos:\r\nsensibilidade: -55db ± 2db\r\ndireção: omnidirecional\r\nimpedância = 2.2k¿\r\ntensão de funcionamento: 1.5v\r\npadrão tensão de funcionamento: 1.5v\r\nresposta de frequência: 50hz-16khz\r\nsnr&gt; 36db\r\ncomprimento do cabo: cerca de 1,8-2 metros\r\naplicação: cantar, gravação em rede, chat on-line, conferência de vídeo online, jogos online, etc.\r\n\r\nEnvio de cores aleatória.', NULL),
(10, '80962147', '2021-01-20 01:00:48', 2, 8, 'Mesa Digitalizadora CTL472 One By Wacom Redwood', 'mesa-digitalizadora-ctl472-one-by-wacom-redwood', 4, 20, 20, 5, '352.28', 1, 1, 4, 1, 'Mesa digitalizadora One by Wacom para desenho digital, edição de fotos, personalizar os documentos, criar a sua própria arte ou apenas adicionar mais diversão para qualquer coisa que você faz no seu PC ou Mac. Sua One by Wacom (CTL472) permite fazer coisas incríveis com o seu computador de um modo mais preciso, conveniente e mais rápido.', NULL),
(11, '72458396', '2021-01-20 01:04:44', 8, 6, 'HD Externo Seagate 1TB Expansion Preto', 'hd-externo-seagate-1tb-expansion-preto', 1, 12, 12, 5, '315.29', 1, 1, 1, 1, 'HD Externo Seagate 1TB Expansion Preto', NULL),
(12, '07164529', '2021-01-20 01:08:34', 9, 4, 'Impressora Laser Hp 1200a', 'impressora-laser-hp-1200a', 30, 80, 60, 50, '1500.18', 1, 1, 5, 1, 'A multifuncional HP Neverstop 1200a é ideal para quem empreende e deseja reduzir custos em impressão em preto. Com ela é possível imprimir grandes quantidades por um custo baixíssimo. Imprima de forma mais rápida e com uma economia ...', NULL),
(41, '08967143', '2021-01-21 15:33:10', 10, 9, 'Lavadora Electrolux LAC13 13 Kg Branca - 110v', 'lavadora-electrolux-lac13-13-kg-branca-110v', 30, 90, 80, 90, '1353.27', 1, 1, 6, 1, 'A Lavadora Electrolux LAC13 conta com tecnologias para trazer muito mais praticidade para o dia a dia na hora da lavagem. Com tecnologia autolimpante Jet&amp;Clean deixa o dispenser perfeito para o próximo ciclo, ou seja, os dias de limpeza do dispenser acabaram. Também vem com um prático filtro pega fiapos, que tira seis vezes mais que outros modelos. E conta com um programa especial para tênis, edredons e peças de cama/banho, que possibilita um tratamento particular para eles, deixando-os limpos e conservados, sem danificá-los. O resultado são roupas mais limpas e menos trabalho pra você.\r\n\r\n- Dispenser Multibox: - Todos os produtos que você precisa para deixar suas roupas limpas e macias em um único compartimento.\r\n\r\n- Dispenser autolimpante com tecnologia Jet&amp;Clean: Jato d’água que dissolve completamente o produto, evitando manchas nas roupas e mantendo o dispenser sempre limpo.\r\n\r\n- Diluição inteligente: Os produtos diluídos são adicionados a água pelas laterais do cesto e não depositados sobre as roupas, evitando manchas. - Novo filtro pega fiapos: Filtro com volume de filtragem 6 vezes maior.Comparado com o volume interno de filtragem do modelo LT12F. Testes segundo procedimentos internos de laboratórios da Electrolux.\r\n\r\n- Turbo Agitação: - Aumenta a velocidade da agitação melhorando o desempenho da lavagem. Indicado para manchas mais difíceisou roupas mais sujas.\r\n\r\n- Reutilização de água: Você pode reutilizar a água da lavagem para outros fins, como lavar o chão.\r\n\r\n- Limpeza do cesto: Essa função faz automaticamente a limpeza da máquina evitando que resíduos de outras lavagens sejam depositados nas roupas.\r\n\r\n- Programa Tênis / Edredom: Programas predeterminados para limpeza de tênis, edredons e cama/banho. Facilitam o trabalho e conservam as suas roupas.\r\n\r\n- Avança etapas - Com uma única tecla, você pode parar ou avançar a programação da máquina e depois retomar de onde parou.\r\n\r\n- Painel intuitivo - Desenvolvido por engenheiros e Consumidores com uma única função: facilitar seu entendimento e antecipar suas necessidades.', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos_fotos`
--

CREATE TABLE `produtos_fotos` (
  `foto_id` int(11) NOT NULL,
  `foto_produto_id` int(11) DEFAULT NULL,
  `foto_caminho` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `produtos_fotos`
--

INSERT INTO `produtos_fotos` (`foto_id`, `foto_produto_id`, `foto_caminho`) VALUES
(30, 4, 'd5c1a09ce0390f449163515f55b2c95b.jpg'),
(38, 9, 'd900eeb848978eb483f51aa39f00d569.jpg'),
(39, 10, '60c2bdeee428b2a4fdb1cbd513f1db4a.jpg'),
(40, 11, 'a47c1d99bc7837fbb8132f33041c6ddf.jpg'),
(43, 9, '<php>foto_caminho; ?&gt;'),
(44, 9, '9057097bb0a0ea90443decc704fff905.jpg'),
(45, 9, 'adaf9f12f3f425b7ec0a8fddd8e197a9.jpg'),
(46, 9, '23c194a0158e4609af139eae81d87ca5.jpg'),
(50, 4, '<php>foto_caminho; ?&gt;'),
(54, 4, '<php>foto_caminho; ?&gt;'),
(55, 4, '<php>foto_caminho; ?&gt;'),
(60, 9, '<php>foto_caminho; ?&gt;'),
(61, 9, '<php>foto_caminho; ?&gt;'),
(62, 9, '<php>foto_caminho; ?&gt;'),
(63, 9, '<php>foto_caminho; ?&gt;'),
(145, 7, '0f1db4a705b18ae4531dbab33baad2ea.jpg'),
(147, 12, '034d93edabc10ad15d6914d81f5d7d2f.jpg'),
(148, 41, 'd665d5f18f12a9bcdaa679e78050ff4e.jpg'),
(149, 8, '8802d3a7db2067c781fcd87a8e713932.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `sistema`
--

CREATE TABLE `sistema` (
  `sistema_id` int(11) NOT NULL,
  `sistema_razao_social` varchar(145) DEFAULT NULL,
  `sistema_nome_fantasia` varchar(145) DEFAULT NULL,
  `sistema_cnpj` varchar(25) DEFAULT NULL,
  `sistema_ie` varchar(25) DEFAULT NULL,
  `sistema_telefone_fixo` varchar(25) DEFAULT NULL,
  `sistema_telefone_movel` varchar(25) NOT NULL,
  `sistema_email` varchar(100) DEFAULT NULL,
  `sistema_site_url` varchar(100) DEFAULT NULL,
  `sistema_cep` varchar(25) DEFAULT NULL,
  `sistema_endereco` varchar(145) DEFAULT NULL,
  `sistema_numero` varchar(25) DEFAULT NULL,
  `sistema_cidade` varchar(45) DEFAULT NULL,
  `sistema_estado` varchar(2) DEFAULT NULL,
  `sistema_produtos_destaques` int(11) NOT NULL,
  `sistema_texto` tinytext DEFAULT NULL,
  `sistema_data_alteracao` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `sistema`
--

INSERT INTO `sistema` (`sistema_id`, `sistema_razao_social`, `sistema_nome_fantasia`, `sistema_cnpj`, `sistema_ie`, `sistema_telefone_fixo`, `sistema_telefone_movel`, `sistema_email`, `sistema_site_url`, `sistema_cep`, `sistema_endereco`, `sistema_numero`, `sistema_cidade`, `sistema_estado`, `sistema_produtos_destaques`, `sistema_texto`, `sistema_data_alteracao`) VALUES
(1, 'Loja virtual Rafael', 'Rafael Guilherme', '80.838.809/0001-26', '683.90228-49', '(11) 2721-2352', '(11) 94570-2236', 'fael3_fael@yahoo.com.br', 'http://vendetudo.com.br', '03934-030', 'Rua Lagoa Bonita', '5', 'São Paulo', 'SP', 6, 'Preço e qualidade!', '2021-01-21 15:48:43');

-- --------------------------------------------------------

--
-- Estrutura da tabela `transacoes`
--

CREATE TABLE `transacoes` (
  `transacao_id` int(11) NOT NULL,
  `transacao_pedido_id` int(11) DEFAULT NULL,
  `transacao_cliente_id` int(11) DEFAULT NULL,
  `transacao_data` timestamp NULL DEFAULT current_timestamp(),
  `transacao_codigo_hash` varchar(255) DEFAULT NULL,
  `transacao_tipo_metodo_pagamento` tinyint(1) DEFAULT NULL COMMENT '1 = Cartão | 2 = Boleto | 3 = Transferência',
  `transacao_codigo_metodo_pagamento` varchar(10) DEFAULT NULL,
  `transacao_link_pagamento` varchar(255) DEFAULT NULL,
  `transacao_banco_escolhido` varchar(20) DEFAULT NULL,
  `transacao_valor_bruto` decimal(15,2) DEFAULT NULL,
  `transacao_valor_taxa_pagseguro` decimal(15,2) DEFAULT NULL,
  `transacao_valor_liquido` decimal(15,2) DEFAULT NULL,
  `transacao_numero_parcelas` int(11) DEFAULT NULL,
  `transacao_valor_parcela` decimal(15,2) DEFAULT NULL,
  `transacao_status` tinyint(1) DEFAULT NULL COMMENT 'Verificar documentação',
  `transacao_data_alteracao` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `transacoes`
--

INSERT INTO `transacoes` (`transacao_id`, `transacao_pedido_id`, `transacao_cliente_id`, `transacao_data`, `transacao_codigo_hash`, `transacao_tipo_metodo_pagamento`, `transacao_codigo_metodo_pagamento`, `transacao_link_pagamento`, `transacao_banco_escolhido`, `transacao_valor_bruto`, `transacao_valor_taxa_pagseguro`, `transacao_valor_liquido`, `transacao_numero_parcelas`, `transacao_valor_parcela`, `transacao_status`, `transacao_data_alteracao`) VALUES
(1, 1, 2, '2021-01-28 17:37:39', '4D10BA4A-E471-48CF-AB60-9E5F415EEC87', 2, '202', 'https://sandbox.pagseguro.uol.com.br/checkout/payment/booklet/print.jhtml?c=b05d64badb2f3645e0a52baf95ee0b05dc6bd9f93712c7ec99a4819a1b2deda09017a1c52cd96086', NULL, '125.32', '5.40', '119.92', 1, '125.32', 1, NULL),
(2, 2, 2, '2021-01-28 17:41:50', '5BAFD21A-B205-464A-B62D-926BF2407A4F', 2, '202', 'https://sandbox.pagseguro.uol.com.br/checkout/payment/booklet/print.jhtml?c=f686021cd6bdf0c6c442c54f36c78d8dcb9d3a8427641896071627e82ad6838230ee4ff2ac9b206d', NULL, '138.72', '5.93', '132.79', 1, '138.72', 1, NULL),
(3, 3, 2, '2021-01-28 17:44:49', '8E1254AB-520D-4B6E-B54A-2B0672F35C67', 2, '202', 'https://sandbox.pagseguro.uol.com.br/checkout/payment/booklet/print.jhtml?c=6bbb89c04ebff554239782413bd8d12e9c658b15caf8049b6912fa0577cc53c8fda1671e8ee1f12e', NULL, '1840.30', '73.83', '1766.47', 1, '1840.30', 1, NULL),
(4, 4, 2, '2021-01-28 17:47:42', '2D4D0E9B-A2D5-4353-A430-522DD1C9C139', 2, '202', 'https://sandbox.pagseguro.uol.com.br/checkout/payment/booklet/print.jhtml?c=7bd02c6ab17b84408e828858df0c4500785063e626678e4e2ffad071ae6562b00a92c5eb3549607b', NULL, '1840.30', '73.83', '1766.47', 1, '1840.30', 1, NULL),
(5, 5, 2, '2021-01-28 18:00:03', 'D95E3FE6-C032-4207-96B0-0BA76FADC44F', 2, '202', 'https://sandbox.pagseguro.uol.com.br/checkout/payment/booklet/print.jhtml?c=de50c9c185f96a9b1a200e09f18dd52fff13051654b29dcc3dc74f6a874263f1c2e0c199613ea251', NULL, '1840.30', '73.83', '1766.47', 1, '1840.30', 1, NULL),
(6, 6, 8, '2021-01-28 18:03:21', '86F5B22A-2872-4F73-B6E4-74EA84F9BF13', 2, '202', 'https://sandbox.pagseguro.uol.com.br/checkout/payment/booklet/print.jhtml?c=75ae9dc0ac5c5896c232129af578572c4f1f2cd64ecc3ccefa317b6a9643c2b34941685d2f7af180', NULL, '1840.30', '73.83', '1766.47', 1, '1840.30', 1, NULL),
(7, 7, 2, '2021-01-28 18:06:12', 'CE75F4E5-D548-491E-90FE-BB004A6906B6', 3, '302', 'https://sandbox.pagseguro.uol.com.br/checkout/payment/eft/print.jhtml?c=814888e6b209d766b0af101fc427160f129c515ddd377add938f74df21cd5c3d21b9b23653acf88e', NULL, '1840.30', '73.83', '1766.47', 1, '1840.30', 1, NULL),
(8, 8, 2, '2021-01-28 23:43:38', 'DD30614C-45E7-4DE2-9FD9-7827308A993E', 2, '202', 'https://sandbox.pagseguro.uol.com.br/checkout/payment/booklet/print.jhtml?c=b112590b3493924249002da3882bcb1e13d5c502803fe2bfcce10db86d6304e4238048af67b73762', NULL, '125.32', '5.40', '119.92', 1, '125.32', 1, NULL),
(9, 9, 2, '2021-01-28 23:48:35', '6432D300-FD08-439D-AC1F-9E4DF36B4650', 2, '202', 'https://sandbox.pagseguro.uol.com.br/checkout/payment/booklet/print.jhtml?c=fa709f3d25883df2aba27bb85f55350319fe7bafbef9b42d01fafce745bb56fc06ad9a9c32b89f86', NULL, '125.32', '5.40', '119.92', 1, '125.32', 1, NULL),
(10, 10, 2, '2021-01-28 23:52:22', '52413729-0D26-4A93-8501-9195F435F567', 2, '202', 'https://sandbox.pagseguro.uol.com.br/checkout/payment/booklet/print.jhtml?c=d65be82b2c763e965736922111891d6140571601af309ab381e441343abd74e73528faf35d5b11c9', NULL, '125.32', '5.40', '119.92', 1, '125.32', 1, NULL),
(11, 11, 2, '2021-01-28 23:55:43', 'F47ED790-32BD-4551-8B08-A3CF9BABDFF8', 2, '202', 'https://sandbox.pagseguro.uol.com.br/checkout/payment/booklet/print.jhtml?c=4b506258b611e571ae980c21c84c8503951bd935be149aa8b0ff6134a6f9e3ab4f8a1a09b0ad23a1', NULL, '125.32', '5.40', '119.92', 1, '125.32', 1, NULL),
(12, 12, 2, '2021-01-28 23:56:44', 'D1D6D488-AF6E-4F6A-BAFC-2B4F1FF34986', 2, '202', 'https://sandbox.pagseguro.uol.com.br/checkout/payment/booklet/print.jhtml?c=bebcf64d4ca082c4ba829251ff015e41e25d6dd83ef91311eb6fd2b3a3c5b78279df6a41f3392717', NULL, '1840.30', '73.83', '1766.47', 1, '1840.30', 1, NULL),
(13, 13, 2, '2021-01-29 00:37:28', 'CE135EBB-CAC8-45BF-939D-CF71F218F35F', 3, '302', 'https://sandbox.pagseguro.uol.com.br/checkout/payment/eft/print.jhtml?c=151e754ece39bac7357b21f5ec1e32867268bb570bd880d4067fa09fcb4cd0989c1f55f39413164c', NULL, '1840.30', '73.83', '1766.47', 1, '1840.30', 1, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `cliente_user_id` int(11) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(254) NOT NULL,
  `activation_selector` varchar(255) DEFAULT NULL,
  `activation_code` varchar(255) DEFAULT NULL,
  `forgotten_password_selector` varchar(255) DEFAULT NULL,
  `forgotten_password_code` varchar(255) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_selector` varchar(255) DEFAULT NULL,
  `remember_code` varchar(255) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `ip_address`, `cliente_user_id`, `username`, `password`, `email`, `activation_selector`, `activation_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `remember_selector`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', NULL, 'administrator', '$2y$12$K3xpa3AHlCRkCXJ40M/PAem5tlbkh3k5hV.UIxp7kBIyr4cGL9E4y', 'admin@admin.com', NULL, '', NULL, NULL, NULL, NULL, NULL, 1268889823, 1611876954, 1, 'Rafael', 'Guilherme', 'ADMIN', '0'),
(4, '::1', NULL, 'Rafael', '$2y$10$6kp1AHZ9VujfpBL5SU5M0u3I4QCTMgi581/FFPfE5yQbCxglWm/a.', 'fael3_fael@yahoo.com.br', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1611020147, NULL, 1, 'Rafael', 'Guilherme', NULL, '(11) 94570-2236'),
(5, '::1', NULL, 'Yasmin', '$2y$10$wuOYf4BbT5gafiQ3PFPSk.97MIbZMKQHHnhpmtaFtB38PL8oo1WmK', 'yasmin.souza@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1611020274, 1611879862, 1, 'Yasmin', 'Souza', NULL, '(11) 99199-9935'),
(6, '::1', NULL, 'cleber', '$2y$10$8Qahbu39itYuX9fzO4Jdc.8GjL96AYw3aKXFavIZ38MFXh0LCXrDS', 'cleber@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1611020542, NULL, 1, 'Cleber', 'Alonso', NULL, NULL),
(7, '', 2, NULL, '', 'yasmyn@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Yasmin', 'Souza', NULL, NULL),
(8, '::1', NULL, 'Isis', '$2y$10$m3O7yGYDTF629FGN0SuK7ueTe.OKEkrxogXUyWLeVRVX.izKlxkZy', 'isis@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1611770372, 1611855345, 1, 'Ísis', 'Nogueira', NULL, NULL),
(9, '::1', 4, 'Freya', '$2y$10$uOg.zfphG/FpBYI6Ax8Zz.nAYpg2bLbRBwddzQl1UfwvRQwsX9sjS', 'freya@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1611770522, NULL, 1, 'Freya', 'Vormir', NULL, '(31) 99735-2458'),
(11, '::1', 6, 'Priscila', '$2y$10$2GaVHfqnC24wlzRLAwYBg.KkSG2Ml.qewowJZuUqX4kLeOD.d6Yj2', 'priscila@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1611851694, NULL, 1, 'Priscila', 'Romero', NULL, '(11) 94570-2232'),
(13, '::1', 8, 'Paula', '$2y$10$UAKSG0DnIS57wUSr5ChqQecBzkVsLuI3c3K45G5jPJJi3HKk4Q8l6', '2paula@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1611856971, NULL, 1, 'Paula', 'Souza', NULL, '(55) 11272-1235');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(13, 1, 1),
(28, 4, 1),
(24, 5, 2),
(25, 6, 1),
(29, 7, 2),
(30, 8, 2),
(31, 9, 2),
(33, 11, 2),
(35, 13, 2);

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`categoria_id`),
  ADD KEY `categoria_pai_id` (`categoria_pai_id`);

--
-- Índices para tabela `categorias_pai`
--
ALTER TABLE `categorias_pai`
  ADD PRIMARY KEY (`categoria_pai_id`);

--
-- Índices para tabela `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`cliente_id`);

--
-- Índices para tabela `config_correios`
--
ALTER TABLE `config_correios`
  ADD PRIMARY KEY (`config_id`);

--
-- Índices para tabela `config_pagseguro`
--
ALTER TABLE `config_pagseguro`
  ADD PRIMARY KEY (`config_id`);

--
-- Índices para tabela `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `marcas`
--
ALTER TABLE `marcas`
  ADD PRIMARY KEY (`marca_id`);

--
-- Índices para tabela `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`pedido_id`),
  ADD KEY `pedido_cliente_id` (`pedido_cliente_id`);

--
-- Índices para tabela `pedidos_produtos`
--
ALTER TABLE `pedidos_produtos`
  ADD KEY `pedido_id` (`pedido_id`,`produto_id`),
  ADD KEY `produto_id` (`produto_id`);

--
-- Índices para tabela `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`produto_id`),
  ADD KEY `produto_categoria_id` (`produto_categoria_id`),
  ADD KEY `produto_marca_id` (`produto_marca_id`);

--
-- Índices para tabela `produtos_fotos`
--
ALTER TABLE `produtos_fotos`
  ADD PRIMARY KEY (`foto_id`),
  ADD KEY `fk_foto_produto_id` (`foto_produto_id`);

--
-- Índices para tabela `transacoes`
--
ALTER TABLE `transacoes`
  ADD PRIMARY KEY (`transacao_id`),
  ADD KEY `transacao_pedido_id` (`transacao_pedido_id`,`transacao_cliente_id`),
  ADD KEY `fk_transacao_cliente_id` (`transacao_cliente_id`);

--
-- Índices para tabela `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_email` (`email`),
  ADD UNIQUE KEY `uc_activation_selector` (`activation_selector`),
  ADD UNIQUE KEY `uc_forgotten_password_selector` (`forgotten_password_selector`),
  ADD UNIQUE KEY `uc_remember_selector` (`remember_selector`),
  ADD KEY `cliente_user_id` (`cliente_user_id`);

--
-- Índices para tabela `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `categorias`
--
ALTER TABLE `categorias`
  MODIFY `categoria_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de tabela `categorias_pai`
--
ALTER TABLE `categorias_pai`
  MODIFY `categoria_pai_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de tabela `clientes`
--
ALTER TABLE `clientes`
  MODIFY `cliente_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de tabela `config_pagseguro`
--
ALTER TABLE `config_pagseguro`
  MODIFY `config_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `marcas`
--
ALTER TABLE `marcas`
  MODIFY `marca_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de tabela `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `pedido_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de tabela `produtos`
--
ALTER TABLE `produtos`
  MODIFY `produto_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT de tabela `produtos_fotos`
--
ALTER TABLE `produtos_fotos`
  MODIFY `foto_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=150;

--
-- AUTO_INCREMENT de tabela `transacoes`
--
ALTER TABLE `transacoes`
  MODIFY `transacao_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de tabela `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de tabela `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `categorias`
--
ALTER TABLE `categorias`
  ADD CONSTRAINT `fk_categoria_pai_id` FOREIGN KEY (`categoria_pai_id`) REFERENCES `categorias_pai` (`categoria_pai_id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `produtos_fotos`
--
ALTER TABLE `produtos_fotos`
  ADD CONSTRAINT `fk_foto_produto_id` FOREIGN KEY (`foto_produto_id`) REFERENCES `produtos` (`produto_id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `transacoes`
--
ALTER TABLE `transacoes`
  ADD CONSTRAINT `fk_transacao_cliente_id` FOREIGN KEY (`transacao_cliente_id`) REFERENCES `clientes` (`cliente_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_transacao_pedido_id` FOREIGN KEY (`transacao_pedido_id`) REFERENCES `pedidos` (`pedido_id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_cliente_user_id` FOREIGN KEY (`cliente_user_id`) REFERENCES `clientes` (`cliente_id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
