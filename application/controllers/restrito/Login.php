<?php

defined('BASEPATH') OR exit('Ação não permitida');

class Login extends CI_Controller {

    public function index() {

        $data = array(
            'titulo' => 'Login na área restrito',
        );


        $this->load->view('restrito/layout/header', $data);
        $this->load->view('restrito/login/index');
        $this->load->view('restrito/layout/footer');
    }

    public function auth() {


        $identity = $this->input->post('email');
        $password = $this->input->post('password');
        $remember = ($this->input->post('remember' ? TRUE : FALSE));


        if ($this->ion_auth->login($identity, $password, $remember)) {
            $this->session->set_flashdata('sucesso', 'Sem muito bem vindo(a)!');
            redirect('restrito');
        } else {
            $this->session->set_flashdata('erro', 'Por favor verifique suas credenciais de acesso');
            redirect('restrito/login');
        }
    }

    public function logout() {

        $this->ion_auth->logout();
        redirect('restrito/login');
    }

}
